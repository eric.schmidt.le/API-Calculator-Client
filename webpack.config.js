var path = require('path');

module.exports = {
    entry: [
        "babel-polyfill",
        "./www/src/main.jsx"
    ],
    output: {
        path: path.join(__dirname, "www/build"),
        publicPath: "build/",
        filename: "build.js"
    },
    module: {
        rules: [
            {test: /\.html$/, loader: "html-loader"},
            {test: /\.styl$/, loader: "style!css!stylus"},
            {test: /\.jsx$/, exclude: /node_modules/, loader: "babel-loader"},
            {test: /\.css$/, loader: "style-loader!css-loader"},
            {test: /\.png$/, loader: "url-loader?mimetype=image/png"}
        ]
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    }
};