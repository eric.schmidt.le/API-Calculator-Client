import Vue from "vue";
import api from "./api/api.jsx";

window.app = new Vue({
    el: '#root',
    data: {
        api: api,
        val1: 6,
        val2: 2,
        placeholder1: 'result =',
        placeholder2: '',
    },
    mounted: function () {
        console.log("vue mounted");
    },
    methods: {
        calcAPI: function (calcOperator) {
            let self = this;
            api.postCalcResource(calcOperator, this.val1, this.val2).then(function (number) {
                self.placeholder2 = number;
                self.val2 = number
            });
            this.val1 = '';
        },
        tausche: function () {
            let change = this.val1;
            this.val1 = this.val2;
            this.val2 = change;
        },
        reinige: function () {
            this.val1 = '';
            this.val2 = '';
        }
    },
});

