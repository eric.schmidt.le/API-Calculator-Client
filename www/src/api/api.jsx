import $ from "jquery";

class API {

    getCalcResource(path) {
        return $.ajax({
            type: 'GET',
            contentType: "application/json",
            url: 'http://localhost:8000/api/v1/' + path,
        });
    }

    postCalcResource(path, n1, n2) {
        return $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: 'http://localhost:8000/api/v1/' + path + '/' + n1 + '/' + n2,
        });
    }

}

export default new API()


