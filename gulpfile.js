var gulp = require('gulp');
var webpack = require('webpack');
var path = require('path');
var open = require('open');
var gutil = require("gulp-util");
var WebpackDevServer = require("webpack-dev-server");
var webpackConfig = require('./webpack.config.js');
var exec = require('child_process').exec;

// semantic source path
var semanticSrc = "./semantic/";

// load gulp tasks of semantic-ui
require(semanticSrc + 'tasks/build');

// do this outside of task, to allow caching
var compiler = webpack(webpackConfig);

// port for testserver
var port = 8080;

// adress of testserver
var address = "http://localhost:" + port + "/webpack-dev-server/index.html";


gulp.task('webpack', function (callback) {
    // run webpack
    webpack(webpackConfig, function (err, stats) {
        if (err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString({
            // output options
        }));
        callback();
    });
});

gulp.task("webpack-dev-server", function (callback) {
    // Start a webpack-dev-server
    new WebpackDevServer(compiler, {
        contentBase: path.join(__dirname, "www"),
        compress: true,
        port: port,
        publicPath: "/build/"
    }).listen(port, "localhost", function (err) {
        if (err) throw new gutil.PluginError("webpack-dev-server", err);
        // Server listening
        gutil.log("[webpack-dev-server]", address);
        open(address);
    });
});

gulp.task("open-unsafe-browser", function () {
    var browser = '\"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe\" --disable-web-security --user-data-dir';

    exec(browser + " " + address);
});

/**
 * Builds the semantic ui files and copies them to scripts/styles
 */
gulp.task('compile-semantic', ['copy-semantic-css', 'copy-semantic-js', 'copy-semantic-assets']);

/**
 * Builds the css file of semantic-ui and copies it to styles
 */
gulp.task('copy-semantic-css', ['build-css'], function () {
    return gulp.src(semanticSrc + 'dist/semantic.min.css')
        .pipe(gulp.dest('www/styles'))
});

/**
 * Builds the javascript file of semantic-ui and copies it to scripts
 */
gulp.task('copy-semantic-js', ['build-javascript'], function () {
    return gulp.src(semanticSrc + 'dist/semantic.min.js')
        .pipe(gulp.dest('www/scripts'));
});

/**
 * Builds the assets of semantic-ui and copies them to styles/themes
 */
gulp.task('copy-semantic-assets', ['build-assets'], function () {
    return gulp.src(semanticSrc + 'dist/themes/**/*')
        .pipe(gulp.dest('www/styles/themes'));
});